# -*- coding: utf-8 -*-
import os
import hashlib


def create_dir(name_dir):
    if not os.path.isdir(name_dir):
        os.mkdir(name_dir)

def save_file(req):
    filename = req.filename
    file_bytes = req.file.read()
    file_hash = hashlib.md5(file_bytes).hexdigest()
    new_filename = file_hash + get_format(filename)
    folder = new_filename[:2] + "/"
    create_dir("app/static/store/" + folder)
    with open("app/static/store/" + folder + new_filename, "wb") as f:
        f.write(file_bytes)
    return file_hash

def download_file(file_hash):
    folder = search_folder(file_hash)
    if not folder:
        return "false"
    else:
        return folder.replace("app","")

def delete_file(file_hash):
    folder = search_folder(file_hash)
    if folder:
        os.remove(folder)
        if not os.listdir(folder[:folder.index(file_hash)]):
            os.rmdir(folder[:folder.index(file_hash)])
    return "true"


def search_folder(file_hash):
    if not file_hash:
        return None
    directory = "app/static/store/" + file_hash[:2]
    if not os.path.isdir(directory):
        return None
    filename = None
    for f in os.listdir(directory):
        if f.startswith(file_hash):
            filename = f
            break
    if not filename:
        return None
    else:
        return directory+"/"+filename


def get_format(filename):
    len_filename = len(filename)-1
    for i in range(len(filename)):
        if filename[len_filename-i] == ".":
            file_type = filename[len_filename-i:]
            return file_type