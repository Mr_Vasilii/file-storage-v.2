import aiohttp_jinja2
import jinja2
from aiohttp import web
from app.other_function.other_functions import download_file, delete_file, save_file, create_dir

routes = web.RouteTableDef()

@routes.get('/')
@aiohttp_jinja2.template('index.html')
async def index_handler(request):
    return {"title":"Хранилище файлов"}

@routes.post('/api/file')
async def api_file(request):
    data = await request.post()
    call_def = tuple((i for i in data))
    items = tuple((v for i, v in data.items()))
    if call_def[0] == "upload":
        return web.Response(text=save_file(data["upload"]))
    elif call_def[0] == "delete":
        return web.Response(text=delete_file(items[0]))
    elif call_def[0] == "download":
        return web.Response(text=download_file(items[0]))
    return web.Response(text="ERROR")


create_dir("app/static/store/")
app = web.Application()
aiohttp_jinja2.setup(app,loader=jinja2.FileSystemLoader('app/templates/'))
app['static_root_url'] = '/static'
app.add_routes(routes)
app.router.add_static('/static', "app/static")

if __name__ == '__main__':
    web.run_app(app)